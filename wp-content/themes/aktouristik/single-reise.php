<?php
get_header(); 
?>

<div class="w-100">
    
    <?php while ( have_posts() ) : the_post();
        $image = get_field('featured_image');
        if( !empty($image) ): ?>
            <img class='ak-reise-img-full' src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
        <?php endif; ?>

        <div class="container mt-5 mb-5">
            <div class="row">
            <div class="col-sm-6">
                <h2 class="reise-title"><?php the_title(); ?></h2>
                <div><?php the_content(); ?></div>
            </div>
            <div class="col-sm-6">
                <p>Reise merken | Reise drucken | Reise als PDF </p>
                <div class="reise-info">
                    <div>
                    <p>Termine</p>
                    <?php
                        // check if the repeater field has rows of data
                        if( have_rows('daten') ):

                            // loop through the rows of data
                            while ( have_rows('daten') ) : the_row();

                                // display a sub field value
                                ?>
                                <p><?php the_sub_field('von'); ?> - <?php the_sub_field('bis');?></p>
                                <?php

                            endwhile;

                        else :

                            // no rows found

                        endif;

                    ?>
                    </div>
                </div>
            </div>
            </div>
        </div> <!-- /.container -->
    <?php endwhile; ?>
</div> <!--/.w-100 -->
    
    

<?php
get_footer();
?>