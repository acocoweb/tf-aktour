
<div class="row m-3">
    <!-- image -->
    <div class="col-sm-6">
    <?php 
    
    $image = get_field('featured_image');
    
    if( !empty($image) ): ?>
    
        <img class='ak-reise-img' src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
    
    <?php endif; ?>

    </div>
    <!-- Title & Excerpt-->
    <div class="col-sm-6">
        <?php the_title( sprintf( '<h2><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
        <p><?php the_field('preis_pp'); ?> € p.P </p>
        <div class="post-excerpt"><?php the_excerpt(); ?></div>
        <a class="btn ak-button" href="<?php the_permalink();?>">Reise ansehen</a>
    </div>
</div> <!-- /.row -->