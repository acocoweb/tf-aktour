<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">

<title><?php echo get_bloginfo( 'name' ); ?></title>


<?php wp_head();?>

</head>

<body>

<!-- Navigation Header -->
<div class="ak-masterhead">
        <div class="mx-auto">
            <nav class="ak-nav">
            <?php wp_nav_menu(
                    array(
                        'menu' => 'haupt-menu',
                        'items_wrap'=>'%3$s',
                        'container' => false,
                        )
                    ); ?>
            </nav>
        </div>
    
</div>

<!-- Start of main contant --> 
<div class="w-100">

    <div class="ak-header">
        <div class="my-auto mx-auto ak-title">
            <h1>
                <a href="<?php echo get_bloginfo( 'wpurl' );?>">
                    <?php echo get_bloginfo( 'name' ); ?>
                </a>
            </h1>
            <p ><?php echo get_bloginfo( 'description' ); ?></p>
        </div>
    </div>

    <!-- this div, has to be placed ONLY for the archives, not for singles 
    removed temporarily
    once the "IF" is programmed, it must be removed from content and archives -->

    <!--<div class="container mt-5 mb-5"> -->
    