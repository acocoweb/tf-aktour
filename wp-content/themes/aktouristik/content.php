<div class="container mt-5 mb-5">
<div class="row m-3">
    <!-- Title & Excerpt-->
    <div class="col-sm-12">
        <h2 ><?php the_title(); ?></h2>
        <p><?php the_date(); ?> by <a href="#"><?php the_author(); ?></a></p>
        <div><?php the_content(); ?></div>
    </div>
</div> <!-- /.row -->