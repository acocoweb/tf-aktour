<div class="container mt-5 mb-5">
<div class="row m-3">
    <!-- Title & Excerpt-->
    <div class="col-sm-12">
        <h1><?php the_title(); ?></h1>
        <div><?php the_content(); ?></div>
    </div>
</div> <!-- /.row -->