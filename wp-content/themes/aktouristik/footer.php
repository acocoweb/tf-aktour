</div><!-- /.w-100 -->
</div><!-- /.container -->

<footer class="ak-footer w-100">
    <div class="mx-auto">
        <p>Template built for <a href="https://transfact.de">Transfact GmbH</a> by <a href="https://acocoweb.com">ACOCO Web</a>.</p>
    </div>
</footer>


<?php wp_footer(); ?> 
</body>
</html>