<?php 
get_header(); 


/* Get The Loop */
	if ( have_posts() ) : while ( have_posts() ) : the_post();
		get_template_part( 'content', get_post_format() );
	endwhile; endif; 


/*  get_sidebar();  */

 get_footer(); 

?>