<?php

function aktouristik_scripts (){
    wp_enqueue_style( 'aktouristik-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );

    wp_enqueue_style( 'aktouristik', get_template_directory_uri() . '/aktouristik.css' );


    wp_enqueue_script( 'aktouristik-bootstrap', get_template_directory_uri() . '/js/bootstrap.bundle.min.js');
    wp_enqueue_script( 'aktouristik-jq', get_template_directory_uri() . '/js/bjquery-3.3.1.min.js');

}   


add_action( 'wp_enqueue_scripts', 'aktouristik_scripts' );

/* Add Featured Images to Trips */
function my_cptui_featured_image_support() {
	add_theme_support( 'post-thumbnails' );
}
add_action( 'after_setup_theme', 'my_cptui_featured_image_support' );

?>